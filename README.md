# Exercise8
Потоки 

#Содержание файла "MemoryDaemon.java"

public class MemoryDaemon implements Runnable {

	public MemoryDaemon() {

	}

	@Override
	public void run() {

	}

}

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise8",
                "component": "maven"
            },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise8",
                "component": "maven"
             },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia test",
            "command": "mvn verify",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise8",
                "component": "maven"
             },
            "problemMatcher": []
        }
    ]
}
